// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: true },
  modules: [
    "@nuxthq/ui",
    "@nuxt/content"
  ],
  ui: {
    icons: ['heroicons', 'ph', 'simple-icons']
  },
  runtimeConfig: {
    gitlabToken: process.env.WEBSITE_GITLAB_TOKEN,
  },
  experimental: {
    componentIslands: true,
  },
  tailwindcss: {
    cssPath: '~/assets/tailwind.css'
  }
})
